import QtQuick 2.6
import Sailfish.Silica 1.0
import "qml/pages"

ApplicationWindow {
    id: app

    allowedOrientations: Orientation.All

    initialPage: Component { MainPage{} }

}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
