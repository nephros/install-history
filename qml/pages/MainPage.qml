/*
 *
 * Copyright 2022,2023 Peter G. (nephros) <sailfish@nephros.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
*/

import QtQuick 2.6
import Sailfish.Silica 1.0
// for LauncherIcon
import Sailfish.Lipstick 1.0
// for LauncherModel, LauncherItem and friends
import org.nemomobile.lipstick 0.1

Page {
    id: page

    allowedOrientations: Orientation.All
    forwardNavigation: !showProblems && pageStack.nextPage() !== null

    property bool unclutter: false
    property bool showLocal: true
    property bool showProblems: false
    property string historyfile: "/var/log/zypp/history"

    property int progress: 0

    //% "Install History"
    //: Application name in Settings and Top Menu
    readonly property string entryName: qsTrId("install-history-entry_name")

    Connections {
        onStatusChanged: {
            if (( status === PageStatus.Deactivating) && detailInfo.expanded ) {
                detailInfo.hide()
            }
            if (pageStack.busy) {
                pageStack.busyChanged.connect(pushStatsPage)
            } else {
                pushStatsPage()
            }
        }
    }

    function pushStatsPage() {
        if ( status === PageStatus.Active && pageStack.nextPage() === null && !pageStack.busy ) {
            pageStack.pushAttached(statsPage);
            pageStack.busyChanged.disconnect(pushStatsPage);
        }
    }

    Component { id: statsPage; StatsPage{ } }

    DockedPanel { id: detailInfo
        z: 10 // this fixes transparency and focus problems

        LauncherItem{ id: appInfo; filePath: "/usr/share/applications/" + detailInfo.appName + ".desktop" }
        property string displayName: appInfo.isValid ? appInfo.title : ""

        property var date
        property var dateTime
        property string appName
        property string version
        property string repo
        property bool install
        property string problem

        dock: Dock.Bottom
        modal: true
        animationDuration : 250
        height: Math.min(content.height+problemCol.height, page.height-Theme.itemSizeLarge)
        width: parent.width
        Rectangle {
            clip: true
            anchors.fill: parent
            anchors.centerIn: parent
            radius: Theme.paddingSmall
            //color: Theme.rgba(Theme.highlightDimmerColor, Theme.opacityOverlay)
            //color: Theme.rgba(Theme.overlayBackgroundColor, Theme.opacityOverlay)
            //color: Theme.rgba(Theme.highlightDimmerFromColor(Theme.secondaryHighlightColor, Theme.colorScheme), Theme.opacityOverlay)
            gradient: Gradient {
                GradientStop { position: 0; color: Theme.rgba(Theme.highlightDimmerFromColor(Theme.secondaryHighlightColor, Theme.colorScheme), Theme.opacityOverlay) }
                GradientStop { position: 2; color: Theme.rgba(Theme.overlayBackgroundColor, Theme.opacityOverlay) }
            }

        }
        Separator { id: sep; anchors.verticalCenter: parent.top; anchors.horizontalCenter: parent.horizontalCenter;
            width: parent.width ; height: Theme.paddingSmall
            color: Theme.primaryColor;
            horizontalAlignment: Qt.AlignHCenter
        }
        Row { id: content
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: sep.bottom
            width: parent.width
            spacing: Theme.paddingMedium
            padding: Theme.paddingLarge
            LauncherIcon { id: icon
                anchors.verticalCenter: detailCol.verticalCenter
                width: size
                icon: appInfo.isValid ? appInfo.iconId : "icon-m-file-rpm"
                opacity: 1.0
                size: Theme.iconSizeLarge
                fillMode: Image.PreserveAspectFit
                cache: true
                smooth: false
                asynchronous: true
            }
            Column { id: detailCol
                width: parent.width - icon.width
                Repeater { model: detailModel; delegate: Component { DetailItem{ width: detailCol.width; alignment: Qt.AlignLeft; visible: val.length > 0; label: lbl; value: val } } }
            }
        }
        Column { id: problemCol
            width: parent.width
            anchors.top: content.bottom
            //anchors.topMargin: Theme.paddingSmall
            SectionHeader{
                visible: detailInfo.problem
                //% "Additional Output:"
                //: Section Header for problems
                text: qsTrId("install-history-additional-output")
                horizontalAlignment: Text.AlignLeft
            }
            TextArea { id: problemText
                visible: detailInfo.problem
                height: Math.min(page.height/2,Theme.itemSizeExtraLarge*4,implicitHeight)
                width: parent.width
                //wrapMode: Text.NoWrap
                text: detailInfo.problem
                color: Theme.primaryColor
                font.pixelSize: Theme.fontSizeExtraSmall
                font.family: "monospace"
                readOnly: true
                rightItem: IconButton {
                    onClicked: Clipboard.text = problemText.text
                    width: icon.width
                    height: icon.height
                    icon.source: "image://theme/icon-m-clipboard"
                }
            }
        }
        ListModel{ id: detailModel }
        onExpandedChanged: updateModel()
        function updateModel() {
            detailModel.clear();
            if (appInfo.isValid) {
                //% "Application"
                //: App Name in the details view
                detailModel.append( { "lbl": qsTrId("install-history-details_application"), "val": detailInfo.displayName });
            }
            //% "Package"
            //: Package name in the details view
            detailModel.append( { "lbl": qsTrId("install-history-details_package"),         "val": detailInfo.appName });
            //% "Version"
            //: Package version in the details view
            detailModel.append( { "lbl": qsTrId("install-history-details_version"),         "val": detailInfo.version });
            detailModel.append( { "lbl": detailInfo.install
                //% "Installed"
                ? qsTrId("install-history-details_installed")
                //% "Removed"
                : qsTrId("install-history-details_removed"),
                "val": Format.formatDate(detailInfo.dateTime, Formatter.DateMedium)
                + " " + Format.formatDate(detailInfo.dateTime, Formatter.TimeValue)
            });
            detailModel.append( {
                //% "Repository"
                "lbl": qsTrId("install-history-repository_name"),
                //% "n/a"
                "val": detailInfo.repo ? detailInfo.repo : qsTrId("install-history-na")
                }
            );
            if (appInfo.isValid) {
                //% "Executes"
                detailModel.append( { "lbl": qsTrId("install-history-details_executes"), "val": appInfo.exec });
            }
            /*
            if (appInfo.isValid) {
                //% "Sandboxed"
                detailModel.append( { "lbl": qsTrId("install-history-details_sandboxed"), "val": detailInfo.appInfo.isSandboxed.toString() });
            }
            */
        }
    }

    PullDownMenu { id: pdp
        flickable: flick
                //% "Refresh"
                MenuItem { text: qsTrId("install-history-menu_refresh");
                    // refresh on delayedclick otherwise the bounce animation freezes while we reload
                    onDelayedClick: { updateHistory(page.historyfile,showLocal);  }
                }
                MenuItem { text: showProblems
                    //% "Clear Problems"
                    ? qsTrId("install-history-menu_clear_problems")
                    //% "Show Problems"
                    : qsTrId("install-history-menu_show_problems");
                    // refresh on delayedclick otherwise the bounce animation freezes while we reload
                    onClicked:      { showProblems = !showProblems }
                    onDelayedClick: {
                        if (showProblems) {
                            getProblems(page.historyfile);
                        } else {
                            updateHistory(page.historyfile,showLocal);
                        }
                    }
                }
                MenuItem { text: showLocal
                    //% "Hide %1"
                    //: show/hide local menu option
                    ? qsTrId("install-history-menu_hidelocal").arg(
                        //% "Local Installs"
                        //: menu option parameter
                        qsTrId("install-history-menu_localinstall")
                        )
                    //% "Show %1"
                    //: show/hide local menu option
                    : qsTrId("install-history-menu_showlocal").arg(
                        //% "Local Installs"
                        //: menu option parameter
                        qsTrId("install-history-menu_localinstall")
                        )
                    enabled: !showProblems
                    // refresh on delayedclick otherwise the bounce animation freezes while we reload
                    onClicked:      { showLocal = !showLocal }
                    onDelayedClick: { updateHistory(page.historyfile,showLocal);  }
                }
                MenuItem { text: page.unclutter
                    //% "Verbose Display"
                    //: menu option
                    ? qsTrId("install-history-menu_verbose")
                    //% "Reduced Display"
                    //: menu option
                    : qsTrId("install-history-menu_reduced")
                    visible: !showProblems
                    onClicked: page.unclutter = !page.unclutter
                }
                MenuItem { text: dateSearch.active
                    //% "Hide search"
                    ? qsTrId("install-history-menu_search_hide")
                    //% "Search by Date"
                    : qsTrId("install-history-menu_search_bydate")
                    onClicked: {
                        dateSearch.active = !dateSearch.active
                        nameSearch.active = false
                    }
                }
                MenuItem { text: nameSearch.active
                    //% "Hide search"
                    ? qsTrId("install-history-menu_search_hide")
                    //% "Search by Name"
                    : qsTrId("install-history-menu_search_byname")
                    onClicked: {
                        nameSearch.active = !nameSearch.active
                        dateSearch.active = false
                    }
                }
    }

    SilicaFlickable { id: flick
        anchors.fill: parent
        PageHeader { id: header;
            //% "Install History"
            //: Application name in main Page header, shoud be the same as "install-history-entry_name" if possible
            title: qsTrId("install-history-header_name")
            description: (appHistoryModel.count > 0)
                //% "%Ln event(s)"
                //: "very, very unlikely to have only one, still, plurals please!"
                ? qsTrId("install-history-header_description", appHistoryModel.count)
                //% "Loading…"
                : qsTrId("install-history-status_loading")
        }
        Column {
            id: searchBar
            anchors.top: header.bottom
            width: parent.width
            SearchField { id: dateSearch
                active: false
                width: parent.width - Theme.horizontalPageMargin
                readOnly: true
                //% "Date"
                placeholderText: view.jumpDate != null ? view.jumpDate.toISOString().substr(0,10) : qsTrId("install-history-date")
                onClicked: {
                    var dialog = pageStack.push(datePicker)
                    dialog.accepted.connect( function() { view.jumpDate = dialog.date; })
                }
                Separator { anchors.verticalCenter: parent.bottom; width: parent.width; color: Theme.primaryColor;}
            }
            SearchField { id: nameSearch
                active: false
                width: parent.width - Theme.horizontalPageMargin
                //% "Name"
                placeholderText: qsTrId("install-history-name")
                inputMethodHints: Qt.ImhNoAutoUppercase
                EnterKey.enabled: text.length > 3
                EnterKey.iconSource: "image://theme/icon-m-enter-next"
                EnterKey.onClicked: view.findNames(text)
                onVisibleChanged: { focus = visible }
                Separator { anchors.verticalCenter: parent.bottom; width: parent.width; color: Theme.primaryColor;}
            }
        }

        SilicaListView { id: view
            anchors.top: searchBar.bottom
            height: parent.height - (header.height + searchBar.height)
            width: parent.width - Theme.horizontalPageMargin
            anchors.horizontalCenter: parent.horizontalCenter
            cacheBuffer: page.height * 2
            populate: Transition { NumberAnimation { properties: "y"; from: 100;  duration: 600 } }
            clip: true
            spacing: Theme.paddingMedium
            model: appHistoryModel
            section {
                labelPositioning: page.unclutter ? ViewSection.CurrentLabelAtStart : ViewSection.InlineLabels
                property: "date"
                delegate: SectionHeader {
                    text: Format.formatDate(section, Formatter.TimepointSectionRelative)
                    font.capitalization: Font.Capitalize
                }
            }

            highlight: highlightBar
            currentIndex: -1

            property var jumpDate: null
            onJumpDateChanged: if (jumpDate != null) { view.jumpTo(jumpDate) }
            /*
             * TODO: make this smarter:
             *
             * maybe have a separate object containing a search index with only the first occurrence of each date
             * filled on first use and re-used
             */
            function jumpTo(jumpDate) {
                console.time("Jump search took: ");
                var pos = findIndex(appHistoryModel, function(item) { return item.date.match("^" + jumpDate.toISOString().substr(0,10)) });
                if (pos) {
                    positionViewAtIndex(pos, ListView.Beginning);
                    currentIndex = pos;
                    return;
                } else {
                    var searchDate =  jumpDate;
                    for (i=Number(jumpDate.toISOString().substr(-2)); i > 0; i--) {
                        searchDate = searchDate - 864e5; // remove 86,400,000 milliseconds == one day
                        pos = find(appHistoryModel, function(item) { return item.date.match("^" + jumpDate.toISOString().substr(0,10)) });
                        if (pos) {
                            positionViewAtIndex(pos, ListView.Beginning);
                            currentIndex = pos;
                            return;
                        }
                    }
                }
                console.timeEnd("Jump search took: ");
            }

            function findNames(name) {
                console.time("Name search took: ");
                var pos = findIndex(appHistoryModel, function(item) { return item.appName.match(name) });
                positionViewAtIndex(pos, ListView.Beginning);
                currentIndex = pos;
                console.timeEnd("Name search took: ");
            }
            /* find something, return index */
            function findIndex(model, criteria) {
                for(var i = ((currentIndex > 0) ? currentIndex+1 : 0); i < model.count; ++i) if (criteria(model.get(i))) return i
                return -1
            }

            /* find something, return object */
            function find(model, criteria) {
              for(var i = 0; i < model.count; ++i) if (criteria(model.get(i))) return model.get(i)
              return null
            }

            delegate: Component { id: historyItem
                ListItem { id: li
                    width: ListView.view.width
                    contentHeight: packagerow.height
                    anchors.horizontalCenter: parent.horizontalCenter
                    property bool install: (action == "install")
                    property bool unclutter: page.unclutter
                    property bool hasProblem: !!message
                    menu: Component { ContextMenu {
                            MenuItem { enabled: repoType == "openrepos"
                                //% "Search on %1"
                                text: qsTrId("install-history-menu_search_on").arg("OpenRepos.")
                                onClicked: Qt.openUrlExternally("https://openrepos.net/search/node/" + appName) }
                            MenuItem { enabled: repoType == "chum"
                                //% "Search on %1"
                                text: qsTrId("install-history-menu_search_on").arg("SailfishOS:Chum")
                                onClicked: Qt.openUrlExternally("https://build.sailfishos.org/package/show/sailfishos:chum/" + appName) }
                        }
                    }
                    onClicked: {
                        detailInfo.appName = appName;
                        detailInfo.version = version;
                        detailInfo.repo = repo;
                        detailInfo.date = date;
                        detailInfo.dateTime = dateTime;
                        detailInfo.install = install;
                        detailInfo.problem = message;
                        detailInfo.show();
                    }
                    Rectangle {
                        anchors.fill: parent
                        anchors.centerIn: parent
                        radius: Theme.paddingSmall
                        color: install ? Theme.rgba(Theme.secondaryColor, 0.05) : Theme.rgba(Theme.overlayBackgroundColor, Theme.opacityFaint)
                    }
                    Icon { id: plusicon;
                        anchors.left: parent.left; anchors.verticalCenter: parent.verticalCenter
                        height: unclutter ? Theme.iconSizeSmall : Theme.iconSizeMedium
                        source: install
                            ? "image://theme/icon-m-add?" + Theme.highlightFromColor(Theme.presenceColor(Theme.PresenceAvailable), Theme.colorScheme)
                            : ((action == "remove")
                                ? "image://theme/icon-m-remove?" + Theme.highlightFromColor(Theme.presenceColor(Theme.PresenceAway), Theme.colorScheme)
                                : "image://theme/icon-m-file-rpm?" + Theme.highlightFromColor(Theme.presenceColor(Theme.PresenceOffline), Theme.colorScheme)
                                )
                        //    ? "image://theme/icon-m-warning?" + Theme.highlightFromColor(Theme.presenceColor(Theme.PresenceBusy), Theme.colorScheme)
                        //    : (install ? "image://theme/icon-m-add?" + Theme.highlightFromColor(Theme.presenceColor(Theme.PresenceAvailable), Theme.colorScheme) : "image://theme/icon-m-remove?" + Theme.highlightFromColor(Theme.presenceColor(Theme.PresenceAway), Theme.colorScheme))
                        fillMode: Image.PreserveAspectFit
                    }
                    Row { id: packagerow
                        anchors.verticalCenter: plusicon.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.left: plusicon.right
                        anchors.margins: Theme.paddingSmall
                        width: parent.width - plusicon.width
                        Column { id: col
                            width: parent.width
                            Row { spacing: Theme.paddingSmall
                                visible: li.unclutter
                                // name, version: uncluttered
                                Label{ visible: li.unclutter;
                                    font.pixelSize: Theme.fontSizeSmall; color: Theme.secondaryColor;
                                    horizontalAlignment: install ? Text.AlignLeft : Text.AlignRight
                                    width: Math.min(implicitWidth, col.width * 2/3);
                                    maximumLineCount: 1; truncationMode: TruncationMode.Elide; elide: Text.ElideMiddle;
                                    text: page.isLandscape ?
                                        Format.formatDate(dateTime, Formatter.DateMedium) + " " + Format.formatDate(dateTime, Formatter.TimeValue)
                                      : Format.formatDate(dateTime, Formatter.DateMediumWithoutYear) + " " + Format.formatDate(dateTime, Formatter.TimeValue)
                                }
                                Label{ text: appName ;  maximumLineCount: 1; truncationMode: TruncationMode.Elide; elide: Text.ElideMiddle; horizontalAlignment: install ? Text.AlignLeft : Text.AlignRight; color: install ? Theme.highlightColor : Theme.primaryColor }
                                Label{ visible: page.isLandscape; text: version ;  maximumLineCount: 1; truncationMode: TruncationMode.Elide; elide: Text.ElideLeft; horizontalAlignment: install ? Text.AlignLeft : Text.AlignRight; color: install ? Theme.secondaryHighlightColor : Theme.secondaryColor}
                                Label{ visible: ( install && page.isLandscape)
                                    text: repo; color: Theme.highlightBackgroundColor; font.pixelSize: Theme.fontSizeSmall}
                            }
                            Row { spacing: Theme.paddingSmall
                                visible: !li.unclutter
                                // name, version: default
                                Label{ visible: li.unclutter;
                                    font.pixelSize: Theme.fontSizeSmall; color: Theme.secondaryColor;
                                    horizontalAlignment: install ? Text.AlignLeft : Text.AlignRight
                                    width: Math.min(implicitWidth, col.width * 2/3);
                                    maximumLineCount: 1; truncationMode: TruncationMode.Elide; elide: Text.ElideMiddle;
                                    //text: Format.formatDate(date, Formatter.DateMedium) + " " + Format.formatDate(dateTime, Formatter.TimeValue)
                                    text: page.isLandscape ?
                                        Format.formatDate(dateTime, Formatter.DateMedium) + " " + Format.formatDate(dateTime, Formatter.TimeValue)
                                      : Format.formatDate(dateTime, Formatter.DateMediumWithoutYear) + " " + Format.formatDate(dateTime, Formatter.TimeValue)
                                }
                                Label{                           text: appName ;  maximumLineCount: 1; truncationMode: TruncationMode.Elide; elide: Text.ElideMiddle; horizontalAlignment: install ? Text.AlignLeft : Text.AlignRight; color: install ? Theme.highlightColor : Theme.primaryColor }
                                Label{ visible: !li.unclutter; text: version ;  maximumLineCount: 1; truncationMode: TruncationMode.Elide; elide: Text.ElideLeft; horizontalAlignment: install ? Text.AlignLeft : Text.AlignRight; color: install ? Theme.secondaryHighlightColor : Theme.secondaryColor}
                            }
                            Row { spacing: Theme.paddingSmall
                                visible: !li.unclutter
                                // date
                                Label{ font.pixelSize: Theme.fontSizeSmall; color: Theme.secondaryColor;
                                    horizontalAlignment: install ? Text.AlignLeft : Text.AlignRight
                                    width: Math.min(implicitWidth, col.width * 2/3);
                                    maximumLineCount: 1; truncationMode: TruncationMode.Elide; elide: Text.ElideMiddle;
                                    //text: Format.formatDate(date, Formatter.DateMedium) + " " + Format.formatDate(dateTime, Formatter.TimeValue)
                                    //text: Format.formatDate(date, Formatter.DateMediumWithoutYear) + " " + Format.formatDate(dateTime, Formatter.TimeValue)
                                    text: Format.formatDate(dateTime, Formatter.DateMediumWithoutYear) + " " + Format.formatDate(dateTime, Formatter.TimeValue)
                                }
                                // repo
                                Label{ visible: ( install )
                                    text: repoName  ; horizontalAlignment: install ? Text.AlignLeft : Text.AlignRight; color: Theme.highlightBackgroundColor; font.pixelSize: Theme.fontSizeSmall
                                }
                            }
                            Row { spacing: Theme.paddingSmall
                                visible: ( hasProblem && !li.showProblems )
                                Label{
                                    width: Math.min(implicitWidth, col.width * 2/3);
                                    maximumLineCount: 5; truncationMode: TruncationMode.Elide; elide: Text.ElideRight;
                                    text: message  ; color: Theme.secondaryColor; font.pixelSize: Theme.fontSizeExtraSmall
                                    font.family: "monospace"
                                }
                            }
                        }
                    }
                }
            }
            VerticalScrollDecorator {}
        }
    }

    Component{ id: datePicker; DatePickerDialog {} }

    Component { id: highlightBar
        Rectangle {
            color: Theme.rgba(Theme.highlightBackgroundColor, Theme.opacityFaint);
            border.color: Theme.highlightBackgroundColor
            radius: Theme.paddingSmall
        }
    }

    ListModel { id: appHistoryModel }

    // handle data mangling in a worker script:
    Component.onCompleted: getHistory(page.historyfile, showLocal)

    function getProblems(fileName) {
        if (!worker.ready) { console.warn("worker not ready!") }
        worker.sendMessage({ action: "getProblems", parameter: { model: appHistoryModel, fn: fileName}})
    }

    function getHistory(fileName, filter) {
        if (!worker.ready) { console.warn("worker not ready!") }
        worker.sendMessage({ action: "getHistory", parameter: { model: appHistoryModel, fn: fileName, f: filter}})
    }

    function updateHistory(fileName, filter) {
        appHistoryModel.clear();
        getHistory(fileName, filter);
    }

    WorkerScript { id: worker; source: "../js/worker.js"
        onMessage: function(m) {
            if (m.event === "progress") page.progress = m.percent
        }
    }
}
// vim: expandtab ts=4 st=4 sw=4 filetype=javascript syntax=qml
