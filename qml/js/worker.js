/*
 *
 * Copyright 2022,2023 Peter G. (nephros) <sailfish@nephros.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
*/

.pragma library

WorkerScript.onMessage = function(m){
    if (m.action === "getHistory"){ loadHistory(m.parameter.model, m.parameter.fn, m.parameter.f, false) }
    if (m.action === "getProblems"){ loadHistory(m.parameter.model, m.parameter.fn, false, true) }

    /* load zypper history log file
     * @param fn url    file to load from
     * @param filter    filter callback
     */
    function loadHistory(model, fn, filter, problems) {
        model.clear(); model.sync();
        console.assert((typeof fn !== "undefined"), "Called without filename");
        console.info("Loading history from " + fn);
        console.time("File request took");
        const query = "file://" + fn;
        const r = new XMLHttpRequest()
        r.open('GET', query);
        r.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        r.send();

        r.onreadystatechange = function(event) {
            if (r.readyState == XMLHttpRequest.DONE) {
                console.timeEnd("File request took");
              if (problems){
                fillProblems(model, r.response.split("\n").reverse());
              } else {
                fillModel(model, r.response.split("\n").reverse(),filter);
              }
            }
        }
    }

    /* populate ListModel from payload */
    function fillModel(model, a, filter) {
        console.time("List loaded in");
        console.time("Filtering took");
        const data = a.filter(filterData);
        if (!filter)
            data = data.filter(filterLocal);
        console.timeEnd("Filtering took");
        console.debug("Filtered Lines: " + data.length + "/" + a.length);
        // performance: compile regexp and define and loop variables
        // outside the loop
        // also, looks oldschool to declare all vars at the top.
        const isOpenrepos = /^openrepos-/
        //const jollarepos = /(adaptation0 adaptation1 aliendalvik apps customer-jolla jolla sailfish-eas store xt9)/
        const line;
        const dt;
        const d;
        const t;
        const date;
        const dateTime;
        const repo;
        const repotype = "other";
        const reponame = null;
        const message = "";
        const element = {};
        data.forEach(function(r){
            line = r.split("|");

            // zypper stores local time at the time of the event.
            // see https://codeberg.org/nephros/install-history/issues/16
            dt = line[0].split(" ");
            d = dt[0].split("-"); t = dt[1].split(":");
            // months are counted from 0-11, so do a d[1] - 1
            dateTime  = new Date( d[0], d[1] - 1, d[2], t[0], t[1], t[2], 0);
            // make a second date clamped to 00:00:01 local time,
            // otherwise the section has empty parts for each hour(?)
            const udate = new Date( d[0], d[1] - 1, d[2], 0, 0, 1, 0);
            // convert to local time string
            date = new Date(udate.getTime() - udate.getTimezoneOffset()*60000).toISOString()

            /* assign pretty names:
             *
              grep install /var/log/zypp/history |grep -v PK_TMP_DIR |  awk -F\| '{print $7}'| sort -u | grep -v openrepos

              adaptation0
              adaptation1
              aliendalvik
              customer-jolla
              harbour-storeman-obs
              mentaljam-obs
              sailfish-eas
              sailfishos-chum
              sailfishos-chum-testing
              store
              xt9
              */
            repo = line[6];
            reponame = repo;

            /*
             * this may look like it should be a switch...case block, but actually no.
             */

            // local repo:
            if  ( (repo  === "PK_TMP_DIR") || (repo  === "_tmpRPMcache_") ) {
                repo=qsTr("local", "short name for the 'local' repo");
                reponame=qsTr("Local Installs", "name for the 'local' repo");
                repotype="local";
            // all the Jolla repos:
            } else if (repo === "store") {
                reponame=qsTr("Jolla Store", "name for the jolla store repo");
                repotype="jolla";
            } else if (repo === "jolla") {
                reponame=qsTr("Jolla System", "name for the jolla jolla repo");
                repotype="jolla";
            } else if (repo === "aliendalvik") {
                reponame=qsTr("Android App Support", "name for the android support repo");
                repotype="jolla";
            } else if (repo === "apps") {
                reponame=qsTr("Jolla Applications", "name for the jolla apps repo");
                repotype="jolla";
            } else if (repo === "sailfish-eas") {
                reponame=qsTr("Exchange Feature", "name for the jolla feature repo");
                repotype="jolla";
            } else if (repo === "xt9") {
                reponame=qsTr("XT9 Feature", "name for the jolla feature repo");
                repotype="jolla";
            } else if (repo === "customer-jolla") {
                reponame=qsTr("Jolla Feature", "name for the jolla customer repo");
                repotype="jolla";
            } else if (repo === "adaptation0") {
                reponame=qsTr("Device Adaptation", "name for the jolla adaptation0 repo");
                repotype="jolla";
            } else if (repo === "adaptation1") {
                reponame=qsTr("Device Adaptation", "name for the jolla adaptation1 repo");
                repotype="jolla";
            // Storeman:
            } else if ( (repo === "mentaljam-obs") || (repo === "harbour-storeman-obs") ) {
                reponame=qsTr("Storeman", "name for the storeman repo");
            // Chum:
            } else if (repo === "sailfishos-chum") {
                reponame=qsTr("SailfishOS:Chum", "name for the chum repo");
                repotype="chum";
            } else if (repo === "sailfishos-chum-testing") {
                reponame=qsTr("SailfishOS:Chum Testing", "name for the chum testing repo");
                repotype="chum";
            // MLS:
            } else if (repo === "nubecula-mls-offline-repo") {
                reponame=qsTr("MLS", "name for the Nubecula Offline MLS repo");
            // OpenRepos:
            } else if ( isOpenrepos.test(repo) ) {
                reponame= qsTr("OpenRepos: %1", "prefix for a openrepos repo").arg(repo.replace("openrepos-", ""));
                repotype="openrepos";
            }
            element = {
                "date":     date,
                "dateTime": dateTime,
                "action":   line[1].trim(),
                "appName":  line[2],
                "version":  line[3],
                "repo":     repo,
                "repoName": reponame,
                "repoType": repotype,
                "message": message,
            }
            model.append(element);
            if ( (model.count % 200) === 0) { model.sync(); WorkerScript.sendMessage({ event: "progress", percent: Math.floor(model.count / data.length * 100) }) }
        }); // foreach func end
        console.timeEnd("List loaded in");
        model.sync()
    }
    /* helper to reduce payload */
    function filterData(data) {
        // lines have the format:
        // 20xx-mm-dd HH:MM:SS|install
        // 20xx-mm-dd HH:MM:SS|remove
        // ..................^17
        // everything else is not interesting
        const re = /^20.{17}(\|install|\|remove).*$/;
        return (re.test(data)) ? true : false;
    }
    function filterLocal(data) {
        //const re = /\|PK_TMP_DIR\|/ // pkcon
        //const re = /\|_tmpRPMcache_\|/ // zypper
        const re = /\|(PK_TMP_DIR|_tmpRPMcache_)\|/ // both
        return (!re.test(data)) ? true : false;
    }

    /* Collect Problems from payload */
    function fillProblems(model, a) {
    model.clear(); model.sync();
    console.time("Problem detection took");
    // 2024-01-30 07:14:49|rremove|openrepos-planetos_store|
    // 2024-01-30 08:21:18|command|root@PGXperiiia10|'zypper' 'remove' 'glibc-devel' 'gcc'|
    // # 2024-01-30 08:21:20 gcc-8.3.0-1.7.22.jolla.aarch64 removed ok
    // # Additional rpm output:
    // # /sbin/ldconfig: relative path `0' used to build cache
    // # warning: %postun(gcc-8.3.0-1.7.22.jolla.aarch64) scriptlet failed, exit status 1
    // #
    // 2024-01-30 08:21:20|remove |gcc|8.3.0-1.7.22.jolla|aarch64|root@PGXperiiia10|

    const cre = /^20.{17}(\|install|\|remove).*$/;
    // const cre = /^20.{17}(\|command|\|install|\|remove).*$/;
    const pre = /^#/;
    const ignore = /NOKEY/;
    const data = a
    // TDOO: do we want this?
    //const ignore = /^# warning:.*NOKEY$/;
    for (var i = 0; i < a.length; ++i) {
      if (!cre.test(a[i])) continue
      if (pre.test(a[i+1])) {
        const idx = i+1
        var res = { }
        const line = a[i].split("|");

        // zypper stores local time at the time of the event.
        // see https://codeberg.org/nephros/install-history/issues/16
        const dt = line[0].split(" ");
        const d = dt[0].split("-"); var t = dt[1].split(":");
        // months are counted from 0-11, so do a d[1] - 1
        const dateTime  = new Date( d[0], d[1] - 1, d[2], t[0], t[1], t[2], 0);
        // make a second date clamped to 00:00:01 local time,
        // otherwise the section has empty parts for each hour(?)
        const udate = new Date( d[0], d[1] - 1, d[2], 0, 0, 1, 0);
        // convert to local time string
        var date = new Date(udate.getTime() - udate.getTimezoneOffset()*60000).toISOString()

        res["date"] = date
        res["dateTime"] = dateTime
        res["action"]   = line[1].trim()
        if (res["action"] == "command") {
          const l = a[idx].split(" ")
          res["appName"] = l[l.length-3]
          const ac = l[l.length-2]
          if (ac == "installed") { res["action"] = "install"
          } else if (ac == "removed") { res["action"] = "remove"
          } else { res["action"] = ac }
          res["version"]  = ""
        } else {
          res["appName"]  = line[2]
          res["version"]  = line[3]
        }

        // collect problem text:
        var msg = []
        while (pre.test(a[idx])) {
          if (!/^# $/.test(a[idx])) msg.push( a[idx])
          idx++
        }
        if (!ignore.test(msg.join())) { // skip uninteresting
            res["message"] = msg.reverse().join("\n")
            model.append(res)
        }
      }
    }
    console.timeEnd("Problem detection took");
    model.sync()
    console.info("Found", model.count, "Problems.")
  }

}

// vim: ft=javascript expandtab ts=4 st=2 sw=2
