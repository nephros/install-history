<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name></name>
    <message id="install-history-entry_name">
        <source>Install History</source>
        <extracomment>Application name in Settings and Top Menu</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-additional-output">
        <source>Additional Output:</source>
        <extracomment>Section Header for problems</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-details_application">
        <source>Application</source>
        <extracomment>App Name in the details view</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-details_package">
        <source>Package</source>
        <extracomment>Package name in the details view</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-details_version">
        <source>Version</source>
        <extracomment>Package version in the details view</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-details_installed">
        <source>Installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-details_removed">
        <source>Removed</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-repository_name">
        <source>Repository</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-na">
        <source>n/a</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-details_executes">
        <source>Executes</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-menu_refresh">
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-menu_clear_problems">
        <source>Clear Problems</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-menu_show_problems">
        <source>Show Problems</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-menu_hidelocal">
        <source>Hide %1</source>
        <extracomment>show/hide local menu option</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-menu_localinstall">
        <source>Local Installs</source>
        <extracomment>menu option parameter</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-menu_showlocal">
        <source>Show %1</source>
        <extracomment>show/hide local menu option</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-menu_verbose">
        <source>Verbose Display</source>
        <extracomment>menu option</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-menu_reduced">
        <source>Reduced Display</source>
        <extracomment>menu option</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-menu_search_hide">
        <source>Hide search</source>
        <oldsource>Hide Search</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-menu_search_bydate">
        <source>Search by Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-menu_search_byname">
        <source>Search by Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-header_name">
        <source>Install History</source>
        <extracomment>Application name in main Page header, shoud be the same as &quot;install-history-entry_name&quot; if possible
----------
Application name in statistaics Page header, shoud be the same as &quot;install-history-entry_name&quot; if possible</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-header_description" numerus="yes">
        <source>%Ln event(s)</source>
        <extracomment>&quot;very, very unlikely to have only one, still, plurals please!&quot;</extracomment>
        <translation type="unfinished">
            <numerusform>%Ln event</numerusform>
            <numerusform>%Ln events</numerusform>
        </translation>
    </message>
    <message id="install-history-status_loading">
        <source>Loading…</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-date">
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-name">
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-menu_search_on">
        <source>Search on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-stats_view_repos">
        <source>View repositories</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-stats_view_active">
        <source>Most active packages</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-stats_view_pkgs">
        <source>View packages</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-header_stats_description">
        <source>Statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-stats_percentage_desc">
        <source>Percentages are relative to all recorded installation events. Bars are relative to the item with the highest event count.</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-stats_mostactive">
        <source>Most active Repositories</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-stats_others">
        <source>others</source>
        <extracomment>things that don&apos;t fit in a category</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-stats_events_no" numerus="yes">
        <source>%Ln</source>
        <extracomment>number of events</extracomment>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message id="install-history-stats_events_perc" numerus="yes">
        <source>%Ln%</source>
        <extracomment>percentage</extracomment>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message numerus="yes">
        <source>%Ln event(s)</source>
        <comment>very, very unlikely to have only one, still, plurals please!</comment>
        <translation type="vanished">
            <numerusform>%Ln event</numerusform>
            <numerusform>%Ln events</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>StatsPage</name>
    <message numerus="yes">
        <source>Installation(s): %Ln</source>
        <comment>very, very unlikely to have one or less, still, plurals please!</comment>
        <translation type="obsolete">
            <numerusform>Installation: %Ln</numerusform>
            <numerusform>Installations: %Ln</numerusform>
        </translation>
    </message>
    <message>
        <source>%L1</source>
        <comment>number of events</comment>
        <translation type="vanished">%L1</translation>
    </message>
    <message>
        <source>(%L1%%)</source>
        <comment>percentage in parentheses</comment>
        <translation type="vanished">(%L1%)</translation>
    </message>
    <message>
        <source>(%L1%%)</source>
        <comment>percentage in parentheses, best translate as &apos;(%L1%)&apos;</comment>
        <translation type="vanished">(%L1%)</translation>
    </message>
    <message>
        <source>(%L1%)</source>
        <comment>percentage in parentheses, best translate as &apos;(%L1%)&apos;</comment>
        <translation type="vanished">(%L1%)</translation>
    </message>
    <message>
        <source>%L1%</source>
        <comment>percentage</comment>
        <translation type="obsolete">%L1%</translation>
    </message>
</context>
</TS>
