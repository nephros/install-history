<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv">
<context>
    <name></name>
    <message id="install-history-entry_name">
        <source>Install History</source>
        <extracomment>Application name in Settings and Top Menu</extracomment>
        <translation>Installationshistorik</translation>
    </message>
    <message id="install-history-additional-output">
        <source>Additional Output:</source>
        <extracomment>Section Header for problems</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-details_application">
        <source>Application</source>
        <extracomment>App Name in the details view</extracomment>
        <translation>App</translation>
    </message>
    <message id="install-history-details_package">
        <source>Package</source>
        <extracomment>Package name in the details view</extracomment>
        <translation>Paket</translation>
    </message>
    <message id="install-history-details_version">
        <source>Version</source>
        <extracomment>Package version in the details view</extracomment>
        <translation>Version</translation>
    </message>
    <message id="install-history-details_installed">
        <source>Installed</source>
        <translation>Installerad</translation>
    </message>
    <message id="install-history-details_removed">
        <source>Removed</source>
        <translation>Borttagen</translation>
    </message>
    <message id="install-history-repository_name">
        <source>Repository</source>
        <translation>Förrådsplats</translation>
    </message>
    <message id="install-history-na">
        <source>n/a</source>
        <translation>Ej tillämpligt</translation>
    </message>
    <message id="install-history-details_executes">
        <source>Executes</source>
        <translation>Kör</translation>
    </message>
    <message id="install-history-menu_refresh">
        <source>Refresh</source>
        <translation>Uppdatera</translation>
    </message>
    <message id="install-history-menu_clear_problems">
        <source>Clear Problems</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-menu_show_problems">
        <source>Show Problems</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-menu_hidelocal">
        <source>Hide %1</source>
        <extracomment>show/hide local menu option</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-menu_localinstall">
        <source>Local Installs</source>
        <extracomment>menu option parameter</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-menu_showlocal">
        <source>Show %1</source>
        <extracomment>show/hide local menu option</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-menu_verbose">
        <source>Verbose Display</source>
        <extracomment>menu option</extracomment>
        <translation>Utförlig visning</translation>
    </message>
    <message id="install-history-menu_reduced">
        <source>Reduced Display</source>
        <extracomment>menu option</extracomment>
        <translation>Reducerad visning</translation>
    </message>
    <message id="install-history-menu_search_hide">
        <source>Hide search</source>
        <oldsource>Hide Search</oldsource>
        <translation>Dölj sökfältet</translation>
    </message>
    <message id="install-history-menu_search_bydate">
        <source>Search by Date</source>
        <translation>Sök efter datum</translation>
    </message>
    <message id="install-history-menu_search_byname">
        <source>Search by Name</source>
        <translation>Sök efter namn</translation>
    </message>
    <message id="install-history-header_name">
        <source>Install History</source>
        <extracomment>Application name in main Page header, shoud be the same as &quot;install-history-entry_name&quot; if possible
----------
Application name in statistaics Page header, shoud be the same as &quot;install-history-entry_name&quot; if possible</extracomment>
        <translation>Installationshistorik</translation>
    </message>
    <message id="install-history-header_description" numerus="yes">
        <source>%Ln event(s)</source>
        <extracomment>&quot;very, very unlikely to have only one, still, plurals please!&quot;</extracomment>
        <translation>
            <numerusform>%Ln händelse</numerusform>
            <numerusform>%Ln händelser</numerusform>
        </translation>
    </message>
    <message id="install-history-status_loading">
        <source>Loading…</source>
        <translation>Läsa in</translation>
    </message>
    <message id="install-history-date">
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message id="install-history-name">
        <source>Name</source>
        <translation>Namn</translation>
    </message>
    <message id="install-history-menu_search_on">
        <source>Search on %1</source>
        <translation>Sök på %1</translation>
    </message>
    <message id="install-history-stats_view_repos">
        <source>View repositories</source>
        <translation>Visa förråd</translation>
    </message>
    <message id="install-history-stats_view_active">
        <source>Most active packages</source>
        <translation>Mest aktiva paket</translation>
    </message>
    <message id="install-history-stats_view_pkgs">
        <source>View packages</source>
        <translation>Visa paket</translation>
    </message>
    <message id="install-history-header_stats_description">
        <source>Statistics</source>
        <translation>Statistik</translation>
    </message>
    <message id="install-history-stats_percentage_desc">
        <source>Percentages are relative to all recorded installation events. Bars are relative to the item with the highest event count.</source>
        <translation>Procentsatser är i förhållande till alla registrerade installationshändelser. Staplar är i förhållande till det objekt som har det högsta antalet händelser.</translation>
    </message>
    <message id="install-history-stats_mostactive">
        <source>Most active Repositories</source>
        <translation>Mest aktiva förråd</translation>
    </message>
    <message id="install-history-stats_others">
        <source>others</source>
        <extracomment>things that don&apos;t fit in a category</extracomment>
        <translation>Annat</translation>
    </message>
    <message id="install-history-stats_events_no" numerus="yes">
        <source>%Ln</source>
        <extracomment>number of events</extracomment>
        <translation type="unfinished">
            <numerusform>%Ln</numerusform>
            <numerusform>%Ln</numerusform>
        </translation>
    </message>
    <message id="install-history-stats_events_perc" numerus="yes">
        <source>%Ln%</source>
        <extracomment>percentage</extracomment>
        <translation type="unfinished">
            <numerusform>%Ln</numerusform>
            <numerusform>%Ln</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Install History</source>
        <translation type="vanished">Installationshistorik</translation>
    </message>
    <message>
        <source>%L1 records</source>
        <translation type="vanished">%L1 poster</translation>
    </message>
    <message>
        <source>Jump to…</source>
        <translation type="vanished">Hoppa till…</translation>
    </message>
    <message>
        <source>tap to select</source>
        <translation type="vanished">Tryck för att välja</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="vanished">Datum</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Namn</translation>
    </message>
    <message>
        <source>hide search</source>
        <translation type="vanished">Dölj sök</translation>
    </message>
    <message>
        <source>Search on %1</source>
        <translation type="vanished">Sök på %1</translation>
    </message>
    <message>
        <source>Version</source>
        <translation type="vanished">Version</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation type="vanished">Installerad</translation>
    </message>
    <message>
        <source>Removed</source>
        <translation type="vanished">Borttagen</translation>
    </message>
    <message>
        <source>n/a</source>
        <translation type="vanished">Ej tillämpligt</translation>
    </message>
    <message>
        <source>Package Name</source>
        <translation type="vanished">Paketnamn</translation>
    </message>
    <message>
        <source>Repository</source>
        <translation type="vanished">Förrådsplats</translation>
    </message>
    <message>
        <source>Package</source>
        <translation type="vanished">Paket</translation>
    </message>
    <message>
        <source>Hide search</source>
        <translation type="vanished">Dölj sökfältet</translation>
    </message>
    <message>
        <source>Search by Date</source>
        <translation type="vanished">Sök efter datum</translation>
    </message>
    <message>
        <source>Search by Name</source>
        <translation type="vanished">Sök efter namn</translation>
    </message>
    <message>
        <source>Verbose Display</source>
        <translation type="vanished">Utförlig visning</translation>
    </message>
    <message>
        <source>Reduced Display</source>
        <translation type="vanished">Reducerad visning</translation>
    </message>
    <message>
        <source>local</source>
        <comment>name for a local repo</comment>
        <translation type="vanished">Lokalt</translation>
    </message>
    <message>
        <source>local</source>
        <comment>type of a local repo</comment>
        <translation type="vanished">Lokalt</translation>
    </message>
    <message numerus="yes">
        <source>%Ln event(s)</source>
        <comment>very, very unlikely to have only one, still, plurals please!</comment>
        <translation type="vanished">
            <numerusform>%Ln händelse</numerusform>
            <numerusform>%Ln händelser</numerusform>
        </translation>
    </message>
    <message>
        <source>Application</source>
        <translation type="vanished">App</translation>
    </message>
    <message>
        <source>Executes</source>
        <translation type="vanished">Kör</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="vanished">Uppdatera</translation>
    </message>
    <message>
        <source>Loading…</source>
        <translation type="vanished">Läsa in</translation>
    </message>
    <message>
        <source>local</source>
        <comment>short name for a local repo</comment>
        <translation type="obsolete">Lokalt</translation>
    </message>
    <message>
        <source>Verbose Display</source>
        <comment>menu option</comment>
        <translation type="vanished">Utförlig visning</translation>
    </message>
    <message>
        <source>Reduced Display</source>
        <comment>menu option</comment>
        <translation type="vanished">Reducerad visning</translation>
    </message>
    <message>
        <source>local</source>
        <comment>short name for the &apos;local&apos; repo</comment>
        <translation type="vanished">Lokalt</translation>
    </message>
</context>
<context>
    <name>StatsPage</name>
    <message>
        <source>Install History</source>
        <translation type="vanished">Installationshistorik</translation>
    </message>
    <message>
        <source>(%L1%)</source>
        <comment>percentage in parentheses, best translate as &apos;(%L1%)&apos;</comment>
        <translation type="vanished">(%L1%)</translation>
    </message>
    <message>
        <source>View repositories</source>
        <translation type="vanished">Visa förråd</translation>
    </message>
    <message>
        <source>Crunching Numbers…</source>
        <translation type="vanished">Tuggar siffror…</translation>
    </message>
    <message>
        <source>View packages</source>
        <translation type="vanished">Visa paket</translation>
    </message>
    <message>
        <source>Statistics</source>
        <translation type="vanished">Statistik</translation>
    </message>
    <message>
        <source>Percentages are relative to all recorded installation events. Bars are relative to the item with the highest event count.</source>
        <translation type="vanished">Procentsatser är i förhållande till alla registrerade installationshändelser. Staplar är i förhållande till det objekt som har det högsta antalet händelser.</translation>
    </message>
    <message>
        <source>others</source>
        <comment>things that don&apos;t fit in a category</comment>
        <translation type="vanished">Annat</translation>
    </message>
    <message>
        <source>%L1</source>
        <comment>number of events</comment>
        <translation type="vanished">%L1</translation>
    </message>
    <message>
        <source>Most active packages</source>
        <translation type="vanished">Mest aktiva paket</translation>
    </message>
    <message>
        <source>Most active Repositories</source>
        <translation type="vanished">Mest aktiva förråd</translation>
    </message>
    <message>
        <source>%L1%</source>
        <comment>percentage</comment>
        <translation type="vanished">%L1%</translation>
    </message>
</context>
</TS>
