<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name></name>
    <message id="install-history-entry_name">
        <source>Install History</source>
        <extracomment>Application name in Settings and Top Menu</extracomment>
        <translation>Historique des installations</translation>
    </message>
    <message id="install-history-additional-output">
        <source>Additional Output:</source>
        <extracomment>Section Header for problems</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-details_application">
        <source>Application</source>
        <extracomment>App Name in the details view</extracomment>
        <translation>Application</translation>
    </message>
    <message id="install-history-details_package">
        <source>Package</source>
        <extracomment>Package name in the details view</extracomment>
        <translation>Paquet</translation>
    </message>
    <message id="install-history-details_version">
        <source>Version</source>
        <extracomment>Package version in the details view</extracomment>
        <translation>Version</translation>
    </message>
    <message id="install-history-details_installed">
        <source>Installed</source>
        <translation>Installé</translation>
    </message>
    <message id="install-history-details_removed">
        <source>Removed</source>
        <translation>Supprimé</translation>
    </message>
    <message id="install-history-repository_name">
        <source>Repository</source>
        <translation>Dépôt</translation>
    </message>
    <message id="install-history-na">
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message id="install-history-details_executes">
        <source>Executes</source>
        <translation>Exécute</translation>
    </message>
    <message id="install-history-menu_refresh">
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-menu_clear_problems">
        <source>Clear Problems</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-menu_show_problems">
        <source>Show Problems</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-menu_hidelocal">
        <source>Hide %1</source>
        <extracomment>show/hide local menu option</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-menu_localinstall">
        <source>Local Installs</source>
        <extracomment>menu option parameter</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-menu_showlocal">
        <source>Show %1</source>
        <extracomment>show/hide local menu option</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-menu_verbose">
        <source>Verbose Display</source>
        <extracomment>menu option</extracomment>
        <translation>Affichage verbeux</translation>
    </message>
    <message id="install-history-menu_reduced">
        <source>Reduced Display</source>
        <extracomment>menu option</extracomment>
        <translation>Affichage réduit</translation>
    </message>
    <message id="install-history-menu_search_hide">
        <source>Hide search</source>
        <oldsource>Hide Search</oldsource>
        <translation>Masquer la recherche</translation>
    </message>
    <message id="install-history-menu_search_bydate">
        <source>Search by Date</source>
        <translation>Rechercher par date</translation>
    </message>
    <message id="install-history-menu_search_byname">
        <source>Search by Name</source>
        <translation>Rechercher par nom</translation>
    </message>
    <message id="install-history-header_name">
        <source>Install History</source>
        <extracomment>Application name in main Page header, shoud be the same as &quot;install-history-entry_name&quot; if possible
----------
Application name in statistaics Page header, shoud be the same as &quot;install-history-entry_name&quot; if possible</extracomment>
        <translation>Historique des installations</translation>
    </message>
    <message id="install-history-header_description" numerus="yes">
        <source>%Ln event(s)</source>
        <extracomment>&quot;very, very unlikely to have only one, still, plurals please!&quot;</extracomment>
        <translation>
            <numerusform>%Ln événement</numerusform>
            <numerusform>%Ln événements</numerusform>
        </translation>
    </message>
    <message id="install-history-status_loading">
        <source>Loading…</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-date">
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message id="install-history-name">
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message id="install-history-menu_search_on">
        <source>Search on %1</source>
        <translation>Rechercher sur %1</translation>
    </message>
    <message id="install-history-stats_view_repos">
        <source>View repositories</source>
        <translation>Voir les dépôts</translation>
    </message>
    <message id="install-history-stats_view_active">
        <source>Most active packages</source>
        <translation>Paquets les plus actifs</translation>
    </message>
    <message id="install-history-stats_view_pkgs">
        <source>View packages</source>
        <translation>Voir les paquets</translation>
    </message>
    <message id="install-history-header_stats_description">
        <source>Statistics</source>
        <translation>Statistiques</translation>
    </message>
    <message id="install-history-stats_percentage_desc">
        <source>Percentages are relative to all recorded installation events. Bars are relative to the item with the highest event count.</source>
        <translation>Pourcentages relatifs à tous les événements d&apos;installation enregistrés. Les barres sont relatives à l&apos;élément ayant le nombre d&apos;événements le plus élevé.</translation>
    </message>
    <message id="install-history-stats_mostactive">
        <source>Most active Repositories</source>
        <translation>Dépôts les plus actifs</translation>
    </message>
    <message id="install-history-stats_others">
        <source>others</source>
        <extracomment>things that don&apos;t fit in a category</extracomment>
        <translation>autres</translation>
    </message>
    <message id="install-history-stats_events_no" numerus="yes">
        <source>%Ln</source>
        <extracomment>number of events</extracomment>
        <translation type="unfinished">
            <numerusform>%Ln</numerusform>
            <numerusform>%Ln</numerusform>
        </translation>
    </message>
    <message id="install-history-stats_events_perc" numerus="yes">
        <source>%Ln%</source>
        <extracomment>percentage</extracomment>
        <translation type="unfinished">
            <numerusform>%Ln</numerusform>
            <numerusform>%Ln</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message numerus="yes">
        <source>%Ln event(s)</source>
        <comment>very, very unlikely to have only one, still, plurals please!</comment>
        <translation type="vanished">
            <numerusform>%Ln événement</numerusform>
            <numerusform>%Ln événements</numerusform>
        </translation>
    </message>
    <message>
        <source>Package</source>
        <translation type="vanished">Paquet</translation>
    </message>
    <message>
        <source>Version</source>
        <translation type="vanished">Version</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation type="vanished">Installé</translation>
    </message>
    <message>
        <source>Removed</source>
        <translation type="vanished">Supprimé</translation>
    </message>
    <message>
        <source>Repository</source>
        <translation type="vanished">Dépôt</translation>
    </message>
    <message>
        <source>n/a</source>
        <translation type="vanished">n/a</translation>
    </message>
    <message>
        <source>Verbose Display</source>
        <translation type="vanished">Affichage verbeux</translation>
    </message>
    <message>
        <source>Reduced Display</source>
        <translation type="vanished">Affichage réduit</translation>
    </message>
    <message>
        <source>Hide search</source>
        <translation type="vanished">Masquer la recherche</translation>
    </message>
    <message>
        <source>Search by Date</source>
        <translation type="vanished">Rechercher par date</translation>
    </message>
    <message>
        <source>Search by Name</source>
        <translation type="vanished">Rechercher par nom</translation>
    </message>
    <message>
        <source>Install History</source>
        <translation type="vanished">Historique des installations</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="vanished">Date</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Nom</translation>
    </message>
    <message>
        <source>Search on %1</source>
        <translation type="vanished">Rechercher sur %1</translation>
    </message>
    <message>
        <source>local</source>
        <comment>name for a local repo</comment>
        <translation type="vanished">local</translation>
    </message>
    <message>
        <source>local</source>
        <comment>type of a local repo</comment>
        <translation type="vanished">local</translation>
    </message>
    <message>
        <source>Application</source>
        <translation type="vanished">Application</translation>
    </message>
    <message>
        <source>Executes</source>
        <translation type="vanished">Exécute</translation>
    </message>
    <message>
        <source>Verbose Display</source>
        <comment>menu option</comment>
        <translation type="obsolete">Affichage verbeux</translation>
    </message>
    <message>
        <source>Reduced Display</source>
        <comment>menu option</comment>
        <translation type="obsolete">Affichage réduit</translation>
    </message>
</context>
<context>
    <name>StatsPage</name>
    <message numerus="yes">
        <source>Installation(s): %Ln</source>
        <comment>very, very unlikely to have one or less, still, plurals please!</comment>
        <translation type="obsolete">
            <numerusform>Installation: %Ln</numerusform>
            <numerusform>Installations: %Ln</numerusform>
        </translation>
    </message>
    <message>
        <source>%L1</source>
        <comment>number of events</comment>
        <translation type="vanished">%L1</translation>
    </message>
    <message>
        <source>(%L1%%)</source>
        <comment>percentage in parentheses</comment>
        <translation type="vanished">(%L1%)</translation>
    </message>
    <message>
        <source>(%L1%%)</source>
        <comment>percentage in parentheses, best translate as &apos;(%L1%)&apos;</comment>
        <translation type="vanished">(%L1%)</translation>
    </message>
    <message>
        <source>(%L1%)</source>
        <comment>percentage in parentheses, best translate as &apos;(%L1%)&apos;</comment>
        <translation type="vanished">(%L1%)</translation>
    </message>
    <message>
        <source>View repositories</source>
        <translation type="vanished">Voir les dépôts</translation>
    </message>
    <message>
        <source>Crunching Numbers…</source>
        <translation type="vanished">Calcul des chiffres…</translation>
    </message>
    <message>
        <source>View packages</source>
        <translation type="vanished">Voir les paquets</translation>
    </message>
    <message>
        <source>Install History</source>
        <translation type="vanished">Historique des installations</translation>
    </message>
    <message>
        <source>Statistics</source>
        <translation type="vanished">Statistiques</translation>
    </message>
    <message>
        <source>Percentages are relative to all recorded installation events. Bars are relative to the item with the highest event count.</source>
        <translation type="vanished">Pourcentages relatifs à tous les événements d&apos;installation enregistrés. Les barres sont relatives à l&apos;élément ayant le nombre d&apos;événements le plus élevé.</translation>
    </message>
    <message>
        <source>others</source>
        <comment>things that don&apos;t fit in a category</comment>
        <translation type="vanished">autres</translation>
    </message>
    <message>
        <source>Most active packages</source>
        <translation type="vanished">Paquets les plus actifs</translation>
    </message>
    <message>
        <source>Most active Repositories</source>
        <translation type="vanished">Dépôts les plus actifs</translation>
    </message>
    <message>
        <source>%L1%</source>
        <comment>percentage</comment>
        <translation type="obsolete">%L1%</translation>
    </message>
</context>
</TS>
