<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name></name>
    <message id="install-history-entry_name">
        <source>Install History</source>
        <extracomment>Application name in Settings and Top Menu</extracomment>
        <translation>История установки пакетов</translation>
    </message>
    <message id="install-history-additional-output">
        <source>Additional Output:</source>
        <extracomment>Section Header for problems</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-details_application">
        <source>Application</source>
        <extracomment>App Name in the details view</extracomment>
        <translation>Приложение</translation>
    </message>
    <message id="install-history-details_package">
        <source>Package</source>
        <extracomment>Package name in the details view</extracomment>
        <translation>Пакет</translation>
    </message>
    <message id="install-history-details_version">
        <source>Version</source>
        <extracomment>Package version in the details view</extracomment>
        <translation>Версия</translation>
    </message>
    <message id="install-history-details_installed">
        <source>Installed</source>
        <translation>Установлен</translation>
    </message>
    <message id="install-history-details_removed">
        <source>Removed</source>
        <translation>Удалён</translation>
    </message>
    <message id="install-history-repository_name">
        <source>Repository</source>
        <translation>Репозиторий</translation>
    </message>
    <message id="install-history-na">
        <source>n/a</source>
        <translation>неизвестно</translation>
    </message>
    <message id="install-history-details_executes">
        <source>Executes</source>
        <translation>Команда запуска</translation>
    </message>
    <message id="install-history-menu_refresh">
        <source>Refresh</source>
        <translation>перезарядить</translation>
    </message>
    <message id="install-history-menu_clear_problems">
        <source>Clear Problems</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-menu_show_problems">
        <source>Show Problems</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-menu_hidelocal">
        <source>Hide %1</source>
        <extracomment>show/hide local menu option</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-menu_localinstall">
        <source>Local Installs</source>
        <extracomment>menu option parameter</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-menu_showlocal">
        <source>Show %1</source>
        <extracomment>show/hide local menu option</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="install-history-menu_verbose">
        <source>Verbose Display</source>
        <extracomment>menu option</extracomment>
        <translation>Обычный список</translation>
    </message>
    <message id="install-history-menu_reduced">
        <source>Reduced Display</source>
        <extracomment>menu option</extracomment>
        <translation>Компактный список</translation>
    </message>
    <message id="install-history-menu_search_hide">
        <source>Hide search</source>
        <oldsource>Hide Search</oldsource>
        <translation>Скрыть поиск</translation>
    </message>
    <message id="install-history-menu_search_bydate">
        <source>Search by Date</source>
        <translation>Искать по дате</translation>
    </message>
    <message id="install-history-menu_search_byname">
        <source>Search by Name</source>
        <translation>Искать по названию</translation>
    </message>
    <message id="install-history-header_name">
        <source>Install History</source>
        <extracomment>Application name in main Page header, shoud be the same as &quot;install-history-entry_name&quot; if possible
----------
Application name in statistaics Page header, shoud be the same as &quot;install-history-entry_name&quot; if possible</extracomment>
        <translation>История установки пакетов</translation>
    </message>
    <message id="install-history-header_description" numerus="yes">
        <source>%Ln event(s)</source>
        <extracomment>&quot;very, very unlikely to have only one, still, plurals please!&quot;</extracomment>
        <translation>
            <numerusform>%Ln действие</numerusform>
            <numerusform>%Ln действия</numerusform>
            <numerusform>%Ln действий</numerusform>
        </translation>
    </message>
    <message id="install-history-status_loading">
        <source>Loading…</source>
        <translation>загрузка данных</translation>
    </message>
    <message id="install-history-date">
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message id="install-history-name">
        <source>Name</source>
        <translation>Название</translation>
    </message>
    <message id="install-history-menu_search_on">
        <source>Search on %1</source>
        <translation>Поиск в %1</translation>
    </message>
    <message id="install-history-stats_view_repos">
        <source>View repositories</source>
        <translation>Просмотр репозиториев</translation>
    </message>
    <message id="install-history-stats_view_active">
        <source>Most active packages</source>
        <translation>Самые активные пакеты</translation>
    </message>
    <message id="install-history-stats_view_pkgs">
        <source>View packages</source>
        <translation>Просмотр пакетов</translation>
    </message>
    <message id="install-history-header_stats_description">
        <source>Statistics</source>
        <translation>Статистика</translation>
    </message>
    <message id="install-history-stats_percentage_desc">
        <source>Percentages are relative to all recorded installation events. Bars are relative to the item with the highest event count.</source>
        <translation>Процент считается от числа всех действий по установке пакетов. Полоски соотносятся с данными о пакете, с которым совершено наибольшее число действий.</translation>
    </message>
    <message id="install-history-stats_mostactive">
        <source>Most active Repositories</source>
        <translation>Самые активные репозитории</translation>
    </message>
    <message id="install-history-stats_others">
        <source>others</source>
        <extracomment>things that don&apos;t fit in a category</extracomment>
        <translation>прочее</translation>
    </message>
    <message id="install-history-stats_events_no" numerus="yes">
        <source>%Ln</source>
        <extracomment>number of events</extracomment>
        <translation type="unfinished">
            <numerusform>%Ln</numerusform>
            <numerusform>%Ln</numerusform>
            <numerusform>%Ln</numerusform>
        </translation>
    </message>
    <message id="install-history-stats_events_perc" numerus="yes">
        <source>%Ln%</source>
        <extracomment>percentage</extracomment>
        <translation type="unfinished">
            <numerusform>%Ln</numerusform>
            <numerusform>%Ln</numerusform>
            <numerusform>%Ln</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Package</source>
        <translation type="vanished">Пакет</translation>
    </message>
    <message>
        <source>Version</source>
        <translation type="vanished">Версия</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation type="vanished">Установлен</translation>
    </message>
    <message>
        <source>Removed</source>
        <translation type="vanished">Удалён</translation>
    </message>
    <message>
        <source>Repository</source>
        <translation type="vanished">Репозиторий</translation>
    </message>
    <message>
        <source>n/a</source>
        <translation type="vanished">неизвестно</translation>
    </message>
    <message>
        <source>Hide search</source>
        <translation type="vanished">Скрыть поиск</translation>
    </message>
    <message>
        <source>Search by Date</source>
        <translation type="vanished">Искать по дате</translation>
    </message>
    <message>
        <source>Search by Name</source>
        <translation type="vanished">Искать по названию</translation>
    </message>
    <message>
        <source>Install History</source>
        <translation type="vanished">История установки пакетов</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="vanished">Дата</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Название</translation>
    </message>
    <message>
        <source>Search on %1</source>
        <translation type="vanished">Поиск в %1</translation>
    </message>
    <message numerus="yes">
        <source>%Ln event(s)</source>
        <comment>very, very unlikely to have only one, still, plurals please!</comment>
        <translation type="vanished">
            <numerusform>%Ln действие</numerusform>
            <numerusform>%Ln действия</numerusform>
            <numerusform>%Ln действий</numerusform>
        </translation>
    </message>
    <message>
        <source>Verbose Display</source>
        <translation type="vanished">Обычный список</translation>
    </message>
    <message>
        <source>Reduced Display</source>
        <translation type="vanished">Компактный список</translation>
    </message>
    <message>
        <source>local</source>
        <comment>name for a local repo</comment>
        <translation type="vanished">local</translation>
    </message>
    <message>
        <source>local</source>
        <comment>type of a local repo</comment>
        <translation type="vanished">локальный</translation>
    </message>
    <message>
        <source>Application</source>
        <translation type="vanished">Приложение</translation>
    </message>
    <message>
        <source>Executes</source>
        <translation type="vanished">Команда запуска</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="vanished">перезарядить</translation>
    </message>
    <message>
        <source>Loading…</source>
        <translation type="vanished">загрузка данных</translation>
    </message>
    <message>
        <source>Verbose Display</source>
        <comment>menu option</comment>
        <translation type="vanished">Обычный список</translation>
    </message>
    <message>
        <source>Reduced Display</source>
        <comment>menu option</comment>
        <translation type="vanished">Компактный список</translation>
    </message>
</context>
<context>
    <name>StatsPage</name>
    <message>
        <source>Install History</source>
        <translation type="vanished">История установки пакетов</translation>
    </message>
    <message>
        <source>Statistics</source>
        <translation type="vanished">Статистика</translation>
    </message>
    <message numerus="yes">
        <source>Installation(s): %Ln</source>
        <comment>very, very unlikely to have one or less, still, plurals please!</comment>
        <translation type="obsolete">
            <numerusform>Установлен: %Ln</numerusform>
            <numerusform>Установлено: %Ln</numerusform>
            <numerusform>Установлено: %Ln</numerusform>
        </translation>
    </message>
    <message>
        <source>View repositories</source>
        <translation type="vanished">Просмотр репозиториев</translation>
    </message>
    <message>
        <source>Active packages</source>
        <translation type="vanished">Активные пакеты</translation>
    </message>
    <message>
        <source>View packages</source>
        <translation type="vanished">Просмотр пакетов</translation>
    </message>
    <message>
        <source>Percentages are relative to all recorded installation events. Bars are relative to the item with the highest event count.</source>
        <translation type="vanished">Процент считается от числа всех действий по установке пакетов. Полоски соотносятся с данными о пакете, с которым совершено наибольшее число действий.</translation>
    </message>
    <message>
        <source>Repositories</source>
        <translation type="vanished">Репозитории</translation>
    </message>
    <message>
        <source>others</source>
        <comment>things that don&apos;t fit in a category</comment>
        <translation type="vanished">прочее</translation>
    </message>
    <message>
        <source>%L1</source>
        <comment>number of events</comment>
        <translation type="vanished">%L1</translation>
    </message>
    <message>
        <source>Crunching Numbers…</source>
        <translation type="vanished">Идёт подсчет…</translation>
    </message>
    <message>
        <source>(%L1%%)</source>
        <comment>percentage in parentheses</comment>
        <translation type="vanished">(%L1%)</translation>
    </message>
    <message>
        <source>(%L1%%)</source>
        <comment>percentage in parentheses, best translate as &apos;(%L1%)&apos;</comment>
        <translation type="vanished">(%L1%)</translation>
    </message>
    <message>
        <source>(%L1%)</source>
        <comment>percentage in parentheses, best translate as &apos;(%L1%)&apos;</comment>
        <translation type="vanished">(%L1%)</translation>
    </message>
    <message>
        <source>Most active packages</source>
        <translation type="vanished">Самые активные пакеты</translation>
    </message>
    <message>
        <source>Most active Repositories</source>
        <translation type="vanished">Самые активные репозитории</translation>
    </message>
    <message>
        <source>%L1%</source>
        <comment>percentage</comment>
        <translation type="vanished">%L1%</translation>
    </message>
</context>
</TS>
