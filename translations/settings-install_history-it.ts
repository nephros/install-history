<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name></name>
    <message id="install-history-entry_name">
        <source>Install History</source>
        <extracomment>Application name in Settings and Top Menu</extracomment>
        <translation>Cronologia installazioni</translation>
    </message>
    <message id="install-history-additional-output">
        <source>Additional Output:</source>
        <extracomment>Section Header for problems</extracomment>
        <translation>Informazioni addizionali:</translation>
    </message>
    <message id="install-history-details_application">
        <source>Application</source>
        <extracomment>App Name in the details view</extracomment>
        <translation>Applicazione</translation>
    </message>
    <message id="install-history-details_package">
        <source>Package</source>
        <extracomment>Package name in the details view</extracomment>
        <translation>Pacchetto</translation>
    </message>
    <message id="install-history-details_version">
        <source>Version</source>
        <extracomment>Package version in the details view</extracomment>
        <translation>Versione</translation>
    </message>
    <message id="install-history-details_installed">
        <source>Installed</source>
        <translation>Installato</translation>
    </message>
    <message id="install-history-details_removed">
        <source>Removed</source>
        <translation>Rimosso</translation>
    </message>
    <message id="install-history-repository_name">
        <source>Repository</source>
        <translation>Repository</translation>
    </message>
    <message id="install-history-na">
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message id="install-history-details_executes">
        <source>Executes</source>
        <translation>Esegue</translation>
    </message>
    <message id="install-history-menu_refresh">
        <source>Refresh</source>
        <translation>Ricarica</translation>
    </message>
    <message id="install-history-menu_clear_problems">
        <source>Clear Problems</source>
        <translation>Pulisci Problemi</translation>
    </message>
    <message id="install-history-menu_show_problems">
        <source>Show Problems</source>
        <translation>Mostra Problemi</translation>
    </message>
    <message id="install-history-menu_hidelocal">
        <source>Hide %1</source>
        <extracomment>show/hide local menu option</extracomment>
        <translation>Nascondi %1</translation>
    </message>
    <message id="install-history-menu_localinstall">
        <source>Local Installs</source>
        <extracomment>menu option parameter</extracomment>
        <translation>Installazioni locali</translation>
    </message>
    <message id="install-history-menu_showlocal">
        <source>Show %1</source>
        <extracomment>show/hide local menu option</extracomment>
        <translation>Mostra %1</translation>
    </message>
    <message id="install-history-menu_verbose">
        <source>Verbose Display</source>
        <extracomment>menu option</extracomment>
        <translation>Visualizzazione verbosa</translation>
    </message>
    <message id="install-history-menu_reduced">
        <source>Reduced Display</source>
        <extracomment>menu option</extracomment>
        <translation>Visualizzazione ridotta</translation>
    </message>
    <message id="install-history-menu_search_hide">
        <source>Hide search</source>
        <oldsource>Hide Search</oldsource>
        <translation>Nascondi ricerca</translation>
    </message>
    <message id="install-history-menu_search_bydate">
        <source>Search by Date</source>
        <translation>Cerca per Data</translation>
    </message>
    <message id="install-history-menu_search_byname">
        <source>Search by Name</source>
        <translation>Cerca per Nome</translation>
    </message>
    <message id="install-history-header_name">
        <source>Install History</source>
        <extracomment>Application name in main Page header, shoud be the same as &quot;install-history-entry_name&quot; if possible
----------
Application name in statistaics Page header, shoud be the same as &quot;install-history-entry_name&quot; if possible</extracomment>
        <translation>Cronologia installazioni</translation>
    </message>
    <message id="install-history-header_description" numerus="yes">
        <source>%Ln event(s)</source>
        <extracomment>&quot;very, very unlikely to have only one, still, plurals please!&quot;</extracomment>
        <translation>
            <numerusform>%Ln evento</numerusform>
            <numerusform>%Ln eventi</numerusform>
        </translation>
    </message>
    <message id="install-history-status_loading">
        <source>Loading…</source>
        <translation>Caricamento...</translation>
    </message>
    <message id="install-history-date">
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message id="install-history-name">
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message id="install-history-menu_search_on">
        <source>Search on %1</source>
        <translation>Cerca su %1</translation>
    </message>
    <message id="install-history-stats_view_repos">
        <source>View repositories</source>
        <translation>Mostra repository</translation>
    </message>
    <message id="install-history-stats_view_active">
        <source>Most active packages</source>
        <translation>Pacchetti più attivi</translation>
    </message>
    <message id="install-history-stats_view_pkgs">
        <source>View packages</source>
        <translation>Vedi pacchetti</translation>
    </message>
    <message id="install-history-header_stats_description">
        <source>Statistics</source>
        <translation>Statistiche</translation>
    </message>
    <message id="install-history-stats_percentage_desc">
        <source>Percentages are relative to all recorded installation events. Bars are relative to the item with the highest event count.</source>
        <translation>Le percentuali sono relative a tutti gli eventi di installazione registrati. Le barre sono relative agli oggetti con maggior numero di eventi.</translation>
    </message>
    <message id="install-history-stats_mostactive">
        <source>Most active Repositories</source>
        <translation>Repository più attivi</translation>
    </message>
    <message id="install-history-stats_others">
        <source>others</source>
        <extracomment>things that don&apos;t fit in a category</extracomment>
        <translation>altro</translation>
    </message>
    <message id="install-history-stats_events_no" numerus="yes">
        <source>%Ln</source>
        <extracomment>number of events</extracomment>
        <translation type="unfinished">
            <numerusform>%Ln</numerusform>
            <numerusform>%Ln</numerusform>
        </translation>
    </message>
    <message id="install-history-stats_events_perc" numerus="yes">
        <source>%Ln%</source>
        <extracomment>percentage</extracomment>
        <translation type="unfinished">
            <numerusform>%Ln</numerusform>
            <numerusform>%Ln</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message numerus="yes">
        <source>%Ln event(s)</source>
        <comment>very, very unlikely to have only one, still, plurals please!</comment>
        <translation type="vanished">
            <numerusform>%Ln evento</numerusform>
            <numerusform>%Ln eventi</numerusform>
        </translation>
    </message>
    <message>
        <source>Package</source>
        <translation type="vanished">Pacchetto</translation>
    </message>
    <message>
        <source>Version</source>
        <translation type="vanished">Versione</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation type="vanished">Installato</translation>
    </message>
    <message>
        <source>Removed</source>
        <translation type="vanished">Rimosso</translation>
    </message>
    <message>
        <source>Repository</source>
        <translation type="vanished">Repository</translation>
    </message>
    <message>
        <source>n/a</source>
        <translation type="vanished">n/a</translation>
    </message>
    <message>
        <source>Hide search</source>
        <translation type="vanished">Nascondi ricerca</translation>
    </message>
    <message>
        <source>Search by Date</source>
        <translation type="vanished">Cerca per Data</translation>
    </message>
    <message>
        <source>Search by Name</source>
        <translation type="vanished">Cerca per Nome</translation>
    </message>
    <message>
        <source>Install History</source>
        <translation type="vanished">Cronologia installazioni</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="vanished">Data</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Nome</translation>
    </message>
    <message>
        <source>Search on %1</source>
        <translation type="vanished">Cerca su %1</translation>
    </message>
    <message>
        <source>Application</source>
        <translation type="vanished">Applicazione</translation>
    </message>
    <message>
        <source>Executes</source>
        <translation type="vanished">Esegue</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="vanished">Ricarica</translation>
    </message>
    <message>
        <source>Loading…</source>
        <translation type="vanished">Caricamento...</translation>
    </message>
    <message>
        <source>Hide %1</source>
        <comment>show/hide local menu option</comment>
        <translation type="vanished">Nascondi %1</translation>
    </message>
    <message>
        <source>Local Installs</source>
        <comment>menu option parameter</comment>
        <translation type="vanished">Installazioni locali</translation>
    </message>
    <message>
        <source>Show %1</source>
        <comment>show/hide local menu option</comment>
        <translation type="vanished">Mostra %1</translation>
    </message>
    <message>
        <source>Verbose Display</source>
        <comment>menu option</comment>
        <translation type="vanished">Visualizzazione verbosa</translation>
    </message>
    <message>
        <source>Reduced Display</source>
        <comment>menu option</comment>
        <translation type="vanished">Visualizzazione ridotta</translation>
    </message>
    <message>
        <source>Additional Output:</source>
        <translation type="vanished">Informazioni addizionali:</translation>
    </message>
    <message>
        <source>Clear Problems</source>
        <translation type="vanished">Pulisci Problemi</translation>
    </message>
    <message>
        <source>Show Problems</source>
        <translation type="vanished">Mostra Problemi</translation>
    </message>
    <message>
        <source>Long-press to copy</source>
        <translation type="vanished">Premi a lungo per copiare</translation>
    </message>
</context>
<context>
    <name>StatsPage</name>
    <message numerus="yes">
        <source>Installation(s): %Ln</source>
        <comment>very, very unlikely to have one or less, still, plurals please!</comment>
        <translation type="obsolete">
            <numerusform>Installation: %Ln</numerusform>
            <numerusform>Installations: %Ln</numerusform>
        </translation>
    </message>
    <message>
        <source>%L1</source>
        <comment>number of events</comment>
        <translation type="vanished">%L1</translation>
    </message>
    <message>
        <source>(%L1%%)</source>
        <comment>percentage in parentheses</comment>
        <translation type="vanished">(%L1%)</translation>
    </message>
    <message>
        <source>(%L1%%)</source>
        <comment>percentage in parentheses, best translate as &apos;(%L1%)&apos;</comment>
        <translation type="vanished">(%L1%)</translation>
    </message>
    <message>
        <source>(%L1%)</source>
        <comment>percentage in parentheses, best translate as &apos;(%L1%)&apos;</comment>
        <translation type="vanished">(%L1%)</translation>
    </message>
    <message>
        <source>View repositories</source>
        <translation type="vanished">Mostra repository</translation>
    </message>
    <message>
        <source>View packages</source>
        <translation type="vanished">Vedi pacchetti</translation>
    </message>
    <message>
        <source>Install History</source>
        <translation type="vanished">Cronologia installazioni</translation>
    </message>
    <message>
        <source>Statistics</source>
        <translation type="vanished">Statistiche</translation>
    </message>
    <message>
        <source>Percentages are relative to all recorded installation events. Bars are relative to the item with the highest event count.</source>
        <translation type="vanished">Le percentuali sono relative a tutti gli eventi di installazione registrati. Le barre sono relative agli oggetti con maggior numero di eventi.</translation>
    </message>
    <message>
        <source>others</source>
        <comment>things that don&apos;t fit in a category</comment>
        <translation type="vanished">altro</translation>
    </message>
    <message>
        <source>Most active packages</source>
        <translation type="vanished">Pacchetti più attivi</translation>
    </message>
    <message>
        <source>Most active Repositories</source>
        <translation type="vanished">Repository più attivi</translation>
    </message>
    <message>
        <source>%L1%</source>
        <comment>percentage</comment>
        <translation type="vanished">%L1%</translation>
    </message>
</context>
</TS>
