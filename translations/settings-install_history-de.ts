<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name></name>
    <message id="install-history-entry_name">
        <source>Install History</source>
        <extracomment>Application name in Settings and Top Menu</extracomment>
        <translation>Installationsverlauf</translation>
    </message>
    <message id="install-history-additional-output">
        <source>Additional Output:</source>
        <extracomment>Section Header for problems</extracomment>
        <translation>Zusätzliche Ausgabe:</translation>
    </message>
    <message id="install-history-details_application">
        <source>Application</source>
        <extracomment>App Name in the details view</extracomment>
        <translation>App</translation>
    </message>
    <message id="install-history-details_package">
        <source>Package</source>
        <extracomment>Package name in the details view</extracomment>
        <translation>Paketname</translation>
    </message>
    <message id="install-history-details_version">
        <source>Version</source>
        <extracomment>Package version in the details view</extracomment>
        <translation>Version</translation>
    </message>
    <message id="install-history-details_installed">
        <source>Installed</source>
        <translation>Installiert</translation>
    </message>
    <message id="install-history-details_removed">
        <source>Removed</source>
        <translation>Entfernt</translation>
    </message>
    <message id="install-history-repository_name">
        <source>Repository</source>
        <translation>Quelle</translation>
    </message>
    <message id="install-history-na">
        <source>n/a</source>
        <translation>n.n.</translation>
    </message>
    <message id="install-history-details_executes">
        <source>Executes</source>
        <translation>Startet</translation>
    </message>
    <message id="install-history-menu_refresh">
        <source>Refresh</source>
        <translation>Neu laden</translation>
    </message>
    <message id="install-history-menu_clear_problems">
        <source>Clear Problems</source>
        <translation>Probleme verbergen</translation>
    </message>
    <message id="install-history-menu_show_problems">
        <source>Show Problems</source>
        <translation>Probleme zeigen</translation>
    </message>
    <message id="install-history-menu_hidelocal">
        <source>Hide %1</source>
        <extracomment>show/hide local menu option</extracomment>
        <translation>%1 verbergen</translation>
    </message>
    <message id="install-history-menu_localinstall">
        <source>Local Installs</source>
        <extracomment>menu option parameter</extracomment>
        <translation>Lokale</translation>
    </message>
    <message id="install-history-menu_showlocal">
        <source>Show %1</source>
        <extracomment>show/hide local menu option</extracomment>
        <translation>%1 anzeigen</translation>
    </message>
    <message id="install-history-menu_verbose">
        <source>Verbose Display</source>
        <extracomment>menu option</extracomment>
        <translation>Auführliche Ansicht</translation>
    </message>
    <message id="install-history-menu_reduced">
        <source>Reduced Display</source>
        <extracomment>menu option</extracomment>
        <translation>Reduzierte Ansicht</translation>
    </message>
    <message id="install-history-menu_search_hide">
        <source>Hide search</source>
        <oldsource>Hide Search</oldsource>
        <translation>Suche ausblenden</translation>
    </message>
    <message id="install-history-menu_search_bydate">
        <source>Search by Date</source>
        <translation>Suche nach Datum</translation>
    </message>
    <message id="install-history-menu_search_byname">
        <source>Search by Name</source>
        <translation>Suche nach Name</translation>
    </message>
    <message id="install-history-header_name">
        <source>Install History</source>
        <extracomment>Application name in main Page header, shoud be the same as &quot;install-history-entry_name&quot; if possible
----------
Application name in statistaics Page header, shoud be the same as &quot;install-history-entry_name&quot; if possible</extracomment>
        <translation>Installationsverlauf</translation>
    </message>
    <message id="install-history-header_description" numerus="yes">
        <source>%Ln event(s)</source>
        <extracomment>&quot;very, very unlikely to have only one, still, plurals please!&quot;</extracomment>
        <translation>
            <numerusform>%Ln Ereignis</numerusform>
            <numerusform>%Ln Ereignisse</numerusform>
        </translation>
    </message>
    <message id="install-history-status_loading">
        <source>Loading…</source>
        <translation>Laden, laden, laden…</translation>
    </message>
    <message id="install-history-date">
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message id="install-history-name">
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message id="install-history-menu_search_on">
        <source>Search on %1</source>
        <translation>Auf %1 suchen</translation>
    </message>
    <message id="install-history-stats_view_repos">
        <source>View repositories</source>
        <translation>Quellen anzeigen</translation>
    </message>
    <message id="install-history-stats_view_active">
        <source>Most active packages</source>
        <translation>Meistgeänderte Pakete</translation>
    </message>
    <message id="install-history-stats_view_pkgs">
        <source>View packages</source>
        <translation>Pakete anzeigen</translation>
    </message>
    <message id="install-history-header_stats_description">
        <source>Statistics</source>
        <translation>Statistiken</translation>
    </message>
    <message id="install-history-stats_percentage_desc">
        <source>Percentages are relative to all recorded installation events. Bars are relative to the item with the highest event count.</source>
        <translation>Prozentangaben relativ zu allen Installationsereignissen. Balken relativ zum Element mit der höchsten Anzahl an Ereignissen.</translation>
    </message>
    <message id="install-history-stats_mostactive">
        <source>Most active Repositories</source>
        <translation>Meistverwendete Quellen</translation>
    </message>
    <message id="install-history-stats_others">
        <source>others</source>
        <extracomment>things that don&apos;t fit in a category</extracomment>
        <translation>sonstige</translation>
    </message>
    <message id="install-history-stats_events_no" numerus="yes">
        <source>%Ln</source>
        <extracomment>number of events</extracomment>
        <translation type="unfinished">
            <numerusform>%Ln</numerusform>
            <numerusform>%Ln</numerusform>
        </translation>
    </message>
    <message id="install-history-stats_events_perc" numerus="yes">
        <source>%Ln%</source>
        <extracomment>percentage</extracomment>
        <translation type="unfinished">
            <numerusform>%Ln</numerusform>
            <numerusform>%Ln</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Install History</source>
        <translation type="vanished">Installationsverlauf</translation>
    </message>
    <message>
        <source>%L1 records</source>
        <translation type="vanished">%L1 Einträge</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="vanished">Datum</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Name</translation>
    </message>
    <message>
        <source>hide search</source>
        <translation type="vanished">Suche ausblenden</translation>
    </message>
    <message>
        <source>Search on %1</source>
        <translation type="vanished">Auf %1 suchen</translation>
    </message>
    <message>
        <source>Jump to…</source>
        <translation type="vanished">Springe zu…</translation>
    </message>
    <message>
        <source>tap to select</source>
        <translation type="vanished">Tippe zum Ändern</translation>
    </message>
    <message>
        <source>Version</source>
        <translation type="vanished">Version</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation type="vanished">Installiert</translation>
    </message>
    <message>
        <source>Removed</source>
        <translation type="vanished">Entfernt</translation>
    </message>
    <message>
        <source>Repository</source>
        <translation type="vanished">Quelle</translation>
    </message>
    <message>
        <source>n/a</source>
        <translation type="vanished">n.n.</translation>
    </message>
    <message>
        <source>Package Name</source>
        <translation type="vanished">Paketname</translation>
    </message>
    <message>
        <source>Hide search</source>
        <translation type="vanished">Suche ausblenden</translation>
    </message>
    <message>
        <source>Search by Date</source>
        <translation type="vanished">Suche nach Datum</translation>
    </message>
    <message>
        <source>Search by Name</source>
        <translation type="vanished">Suche nach Name</translation>
    </message>
    <message>
        <source>Package</source>
        <translation type="vanished">Paketname</translation>
    </message>
    <message>
        <source>Verbose Display</source>
        <translation type="vanished">Auführliche Ansicht</translation>
    </message>
    <message>
        <source>Reduced Display</source>
        <translation type="vanished">Reduzierte Ansicht</translation>
    </message>
    <message>
        <source>local</source>
        <comment>name for a local repo</comment>
        <translation type="vanished">lokal</translation>
    </message>
    <message>
        <source>local</source>
        <comment>type of a local repo</comment>
        <translation type="vanished">lokal</translation>
    </message>
    <message>
        <source>%L1 events</source>
        <translation type="vanished">%L1 Ereignisse</translation>
    </message>
    <message numerus="yes">
        <source>%Ln event(s)</source>
        <comment>very, very unlikely to have only one, still, plurals please!</comment>
        <translation type="vanished">
            <numerusform>%Ln Ereignis</numerusform>
            <numerusform>%Ln Ereignisse</numerusform>
        </translation>
    </message>
    <message>
        <source>Application</source>
        <translation type="vanished">App</translation>
    </message>
    <message>
        <source>Executes</source>
        <translation type="vanished">Startet</translation>
    </message>
    <message>
        <source>No History available</source>
        <translation type="vanished">Kein Verlauf gefunden</translation>
    </message>
    <message>
        <source>The history file is empty or the loading failed</source>
        <translation type="vanished">Entweder fehlt die Datei, oder das Laden ist fehlgeschlagen</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="vanished">Neu laden</translation>
    </message>
    <message>
        <source>Loading…</source>
        <translation type="vanished">Laden, laden, laden…</translation>
    </message>
    <message>
        <source>Hide local</source>
        <translation type="vanished">Lokale verbergen</translation>
    </message>
    <message>
        <source>Show local</source>
        <translation type="vanished">Lokale anzeigen</translation>
    </message>
    <message>
        <source>Jolla Store</source>
        <comment>name for the jolla store repo</comment>
        <translation type="vanished">Jolla Store</translation>
    </message>
    <message>
        <source>Jolla System</source>
        <comment>name for the jolla jolla repo</comment>
        <translation type="vanished">Jolla system</translation>
    </message>
    <message>
        <source>Android App Support</source>
        <comment>name for the android support repo</comment>
        <translation type="vanished">Android Unterstützung</translation>
    </message>
    <message>
        <source>Jolla Applications</source>
        <comment>name for the jolla apps repo</comment>
        <translation type="vanished">Jolla Programme</translation>
    </message>
    <message>
        <source>Exchange Feature</source>
        <comment>name for the jolla feature repo</comment>
        <translation type="vanished">Exchange Feature</translation>
    </message>
    <message>
        <source>XT9 Feature</source>
        <comment>name for the jolla feature repo</comment>
        <translation type="vanished">XT9 Feature</translation>
    </message>
    <message>
        <source>Jolla Feature</source>
        <comment>name for the jolla customer repo</comment>
        <translation type="vanished">Jolla Feature</translation>
    </message>
    <message>
        <source>Device Adaptation</source>
        <comment>name for the jolla adaptation0 repo</comment>
        <translation type="vanished">Geräte-Adaption</translation>
    </message>
    <message>
        <source>Device Adaptation</source>
        <comment>name for the jolla adaptation1 repo</comment>
        <translation type="vanished">Geräte-Adaption</translation>
    </message>
    <message>
        <source>Storeman</source>
        <comment>name for the storeman repo</comment>
        <translation type="vanished">Storeman</translation>
    </message>
    <message>
        <source>SailfishOS:Chum</source>
        <comment>name for the chum repo</comment>
        <translation type="vanished">SailfishOS:Chum</translation>
    </message>
    <message>
        <source>SailfishOS:Chum Testing</source>
        <comment>name for the chum testing repo</comment>
        <translation type="vanished">Sailfishos:Chum Test</translation>
    </message>
    <message>
        <source>MLS</source>
        <comment>name for the Nubecula Offline MLS repo</comment>
        <translation type="vanished">MLS</translation>
    </message>
    <message>
        <source>OpenRepos: %1</source>
        <comment>prefix for a openrepos repo</comment>
        <translation type="vanished">OpenRepos: %1</translation>
    </message>
    <message>
        <source>Package Details</source>
        <translation type="vanished">Paketdtails</translation>
    </message>
    <message>
        <source>Update History</source>
        <translation type="vanished">Updateverlauf</translation>
    </message>
    <message>
        <source>Hide %1</source>
        <comment>show/hide local menu option</comment>
        <translation type="vanished">%1 verbergen</translation>
    </message>
    <message>
        <source>Local Installs</source>
        <comment>menu option parameter</comment>
        <translation type="vanished">Lokale</translation>
    </message>
    <message>
        <source>Show %1</source>
        <comment>show/hide local menu option</comment>
        <translation type="vanished">%1 anzeigen</translation>
    </message>
    <message>
        <source>Verbose Display</source>
        <comment>menu option</comment>
        <translation type="vanished">Auführliche Ansicht</translation>
    </message>
    <message>
        <source>Reduced Display</source>
        <comment>menu option</comment>
        <translation type="vanished">Reduzierte Ansicht</translation>
    </message>
    <message>
        <source>local</source>
        <comment>short name for the &apos;local&apos; repo</comment>
        <translation type="vanished">lokal</translation>
    </message>
    <message>
        <source>Local Installs</source>
        <comment>name for the &apos;local&apos; repo</comment>
        <translation type="vanished">Lokale Installationen</translation>
    </message>
    <message>
        <source>Additional Output:</source>
        <translation type="vanished">Zusätzliche Ausgabe:</translation>
    </message>
    <message>
        <source>Clear Problems</source>
        <translation type="vanished">Probleme verbergen</translation>
    </message>
    <message>
        <source>Show Problems</source>
        <translation type="vanished">Probleme zeigen</translation>
    </message>
    <message>
        <source>Long-press to copy</source>
        <translation type="vanished">Zum kopieren lange tippen und halten</translation>
    </message>
</context>
<context>
    <name>StatsPage</name>
    <message>
        <source>Install History</source>
        <translation type="vanished">Installationsverlauf</translation>
    </message>
    <message>
        <source>Statistics</source>
        <translation type="vanished">Statistiken</translation>
    </message>
    <message>
        <source>Installations: %L1</source>
        <translation type="vanished">Installationen: %L1</translation>
    </message>
    <message>
        <source>(%L1%)</source>
        <translation type="vanished">(L1%)</translation>
    </message>
    <message>
        <source>(%L1%)</source>
        <comment>percentage in parentheses</comment>
        <translation type="vanished">(%L1%)</translation>
    </message>
    <message numerus="yes">
        <source>Installation(s): %Ln</source>
        <comment>very, very unlikely to have one or less, still, plurals please!</comment>
        <translation type="vanished">
            <numerusform>Installation</numerusform>
            <numerusform>Installationen</numerusform>
        </translation>
    </message>
    <message>
        <source>View repositories</source>
        <translation type="vanished">Quellen anzeigen</translation>
    </message>
    <message>
        <source>Active packages</source>
        <translation type="vanished">Paketaktivitäten</translation>
    </message>
    <message>
        <source>View packages</source>
        <translation type="vanished">Pakete anzeigen</translation>
    </message>
    <message>
        <source>Percentages are relative to all recorded installation events. Bars are relative to the item with the highest event count.</source>
        <translation type="vanished">Prozentangaben relativ zu allen Installationsereignissen. Balken relativ zum Element mit der höchsten Anzahl an Ereignissen.</translation>
    </message>
    <message>
        <source>Repositories</source>
        <translation type="vanished">Quellen</translation>
    </message>
    <message>
        <source>others</source>
        <comment>things that don&apos;t fit in a category</comment>
        <translation type="vanished">sonstige</translation>
    </message>
    <message>
        <source>%L1</source>
        <comment>number of events</comment>
        <translation type="vanished">%L1</translation>
    </message>
    <message>
        <source>Crunching Numbers…</source>
        <translation type="vanished">Berechne…</translation>
    </message>
    <message>
        <source>(%L1%%)</source>
        <comment>percentage in parentheses</comment>
        <translation type="vanished">(%L1%)</translation>
    </message>
    <message>
        <source>(%L1%%)</source>
        <comment>percentage in parentheses, best translate as &apos;(%L1%)&apos;</comment>
        <translation type="vanished">(%L1%)</translation>
    </message>
    <message>
        <source>(%L1%)</source>
        <comment>percentage in parentheses, best translate as &apos;(%L1%)&apos;</comment>
        <translation type="vanished">(%L1%)</translation>
    </message>
    <message>
        <source>Most active packages</source>
        <translation type="vanished">Meistgeänderte Pakete</translation>
    </message>
    <message>
        <source>Most active Repositories</source>
        <translation type="vanished">Meistverwendete Quellen</translation>
    </message>
    <message>
        <source>No History available</source>
        <translation type="vanished">Kein Verlauf gefunden</translation>
    </message>
    <message>
        <source>%L1%</source>
        <comment>percentage</comment>
        <translation type="vanished">%L1%</translation>
    </message>
</context>
</TS>
