######################################################################
# This is for translation ONLY, use build.sh for building
######################################################################

TEMPLATE = aux
TARGET = install-history
CONFIG += sailfishapp sailfishapp_i18n

lupdate_only {
    SOURCES += qml/pages/MainPage.qml \
               qml/pages/StatsPage.qml
}

# Input
TRANSLATIONS += translations/settings-install_history-de.ts \
                translations/settings-install_history-en.ts \
                translations/settings-install_history-it.ts \
                translations/settings-install_history-fr.ts \
                translations/settings-install_history-ru.ts \
                translations/settings-install_history-sv.ts
