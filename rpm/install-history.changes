* Fri Nov 15 2024 nephros <sailfish@nephros.org> - 0.11.5
- migrate translations to ID-based
- should fix translations not working

* Thu Aug 07 2024 nephros <sailfish@nephros.org> - 0.11.4
- update install problem view

* Tue Aug 6 2024 nephros <sailfish@nephros.org> - 0.11.3
- add Italian translation (#26)
- translate the Settings Entry as well (#24)

* Thu May 30 19:42:42 2024 nephros <sailfish@nephros.org> - 0.11.2
- Hide docked panel on page transition (#25).

* Thu May 30 2024 nephros <sailfish@nephros.org> - 0.11.1
- add install problem view

* Fri  4 Aug 2023 2023 nephros <sailfish@nephros.org> - 0.10.1
- fix section time display

* Sat Mar 11 2023 nephros <sailfish@nephros.org> - 0.10.0
- use a worker script to mangle the data. 3x speed-up!

* Sun Aug 28 2022 nephros <sailfish@nephros.org> - 0.9.19
- fix license string for OBS

* Sun Aug 28 2022 nephros <sailfish@nephros.org> - 0.9.18
- add/fix zypper version to local install detection

* Thu Apr 24 2022 nephros <sailfish@nephros.org> - 0.9.17
- Pretty names for 'local' repos
- Another date display fix: JS Date Months go fom 0 to 11 :/

* Thu Apr 14 2022 nephros <sailfish@nephros.org> - 0.9.16
- Support hiding local events
- Make repository names pretty
- Fix a bug where the time display was off by a month or so

* Fri Apr  1 2022 nephros <sailfish@nephros.org> - 0.9.15
- Support reloading history file
- French translation by @pherjung, thanks!
- Fix a bug where the time display was off (#16)
- translated category entry

* Tue Feb 22 2022 nephros <sailfish@nephros.org> - 0.9.14
- fix a crash on certain device/version combinations
- improve icon loading
- improve startup times
- remove inline icons
- minor UI tweaks
- translation updates

* Sun Feb 20 2022 nephros <sailfish@nephros.org> - 0.9.13
- make category entry translatable
- improve icon loading
- switch license from "MIT" to Apache 2.0
- minor UI tweaks
- translation updates

* Mon Feb 14 2022 nephros <sailfish@nephros.org> - 0.9.12
- add Statistics
- add Russian translation by @dikonov
- fix Search field display on 3.x

* Mon Feb 14 2022 nephros <sailfish@nephros.org> - 0.9.11-1
- update Swedish translation by @eson

* Mon Feb 14 2022 nephros <sailfish@nephros.org> - 0.9.11
- add Swedish translation by @eson
- introduce Pulley menu
- show/hide search in Pulley menu
- add info window on short tap
- add "compressed display" option
- different section headers
- "small improvements and stability fixes" ;)

* Fri Feb 11 2022 nephros <sailfish@nephros.org> - 0.9.10+git1
- add Swedish translation by eson

* Fri Feb 11 2022 nephros <sailfish@nephros.org> - 0.9.10
- fix scrolling

* Fri Feb 11 2022 nephros <sailfish@nephros.org> - 0.9.9
- introduce translations
- deal with ellipseses
- fix ListView dimensions
- update screenshot

* Fri Feb 11 2022 nephros <sailfish@nephros.org> - 0.9.8
- Improve/Fix search:
- see https://forum.sailfishos.org/t/installation-history-app/10253/9?u=nephros
- search only on Enter
- continuous search, wrap around
- fix result positioning
- focus name search field

* Tue Feb 08 2022 nephros <sailfish@nephros.org> - 0.9.7
- first release on chum
