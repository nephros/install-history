# 
# Do NOT Edit the Auto-generated Part!
# Generated by: spectacle version 0.32
# 

Name:       install-history

# >> macros
# << macros

Summary:    Shows when packages have been installed or uninstalled
Version:    0.11.4
Release:    1
Group:      Applications
License:    ASL 2.0
BuildArch:  noarch
URL:        https://codeberg.org/nephros/install-history
Source0:    %{name}-%{version}.tar.gz
Source100:  install-history.yaml
Requires:   lipstick-qt5
Requires:   sailfishsilica-qt5
Requires:   jolla-settings
BuildRequires:  qt5-qttools-linguist
BuildRequires:  qt5-qmake

%description

%{summary}, and some additional information.

There is no Launcher icon for this app.
It adds an entry in the Settings application under the Info section.

Hint: this means you can have a quick-access shortcut on the Top Menu for it.

%if 0%{?_chum}
Type: desktop-application
DeveloperName: nephros
Categories:
 - PackageManager
 - Settings
 - Utility
Custom:
  Repo: %{url}
PackageIcon: https://sailfishos.org/content/sailfishos-docs//sailfish-content-graphics-default/latest/images/icon-m-file-rpm.svg
Screenshots:
 - %{url}/raw/branch/master/Screenshot_001.jpg
 - %{url}/raw/branch/master/Screenshot_002.jpg
 - %{url}/raw/branch/master/Screenshot_003.jpg
 - %{url}/raw/branch/master/Screenshot_004.jpg
 - %{url}/raw/branch/master/Screenshot_005.jpg
Links:
  Homepage: https://forum.sailfishos.org/t/10253
  Bugtracker: %{url}/issues
%endif


%prep
%setup -q -n %{name}-%{version}

# >> setup
# << setup

%build
# >> build pre
# << build pre



# >> build post
PATH=$PATH:%{_libdir}/qt5/bin
%{_libdir}/qt5/bin/lrelease -idbased -silent *.pro
# << build post

%install
rm -rf %{buildroot}
# >> install pre
# << install pre

# >> install post
%__install -m 644 -D qml/pages/MainPage.qml %{buildroot}%{_datadir}/%{name}/qml/pages/MainPage.qml
%__install -m 644 -D qml/pages/StatsPage.qml %{buildroot}%{_datadir}/%{name}/qml/pages/StatsPage.qml
%__install -m 644 -D qml/js/worker.js %{buildroot}%{_datadir}/%{name}/qml/js/worker.js
%__install -m 644 -D settings/entries/org.nephros.install-history.json %{buildroot}%{_datadir}/jolla-settings/entries/org.nephros.install-history.json
for f in translations/*.qm; do
%__install -m 644 -D ${f} %{buildroot}%{_datadir}/${f}
done
# << install post

%files
%defattr(-,root,root,-)
%license COPYING
%{_datadir}/translations/*.qm
%{_datadir}/%{name}/qml/*
%{_datadir}/jolla-settings/entries/org.nephros.install-history.json
# >> files
# << files
